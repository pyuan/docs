# Installing a vim plugin for html
[Original
Guide](https://medium.com/vim-drops/be-a-html-ninja-with-emmet-for-vim-feee15447ef1)

## Step 1
Install vimplug plugin manager
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
Then run
```
vim -c ":PlugInstall" -c ":qa"
```

## Step 2
Add this to .vimrc
```
call plug#begin('~/.vim/plugged')
call plug#end()
```
In between the two call functions, any vim plugin can be plugged. 

## Step 3
To install the vim plugin for html make sure your vimrc looks like this
```
call plug#begin('~/.vim/plugged')
Plug 'mattn/emmet-vim'
call plug#end()
```

## Step 4
Time to decide what is the "activate key". Put the following in .vimrc. 
The following will set the comma to the activation key
```
let g:user_emmet_leader_key=','
```
The installation is finished. 

# Vim shortcuts
Remember to type `,,` at the end of these next commands to execute.
## Create minimal page
`html:5`
## Add angle brackets around tag
`html`
produces\
`<html></html>`
## Make an element child
`div>a`
produces\
`<div><a href=""></a></div>`\
This can be repeated unlimited amount of times
## Make multiple elements
`div*2`
produces\
`<div></div><div></div>`\
Parenthesis works too\
`(li>a)*3` 
produces
```
<li><a href=""></a></li>
<li><a href=""></a></li>
<li><a href=""></a></li>
```
## Add class or id
`span.your_class`\
produces\
`<span class="your_class"></span>`\
`span#your_id`\
produces\
`<span id="your_id"></span>`
# Final Notes
This will work in other file formats besides .html, so be careful when editing
other types of files or you might find yourself with a minimal page inside
whatever you were working...

# Set custom tabspace in .vimrc
Add this to .vimrc
```
autocmd FileType YOUR_FILE_LANGUAGE setlocal LOCAL_ATTRIBUTES
```
Example
```
autocmd FileType html setlocal ts=2 sts=2 sw=2
```
## Enable file type plugin (OLD METHOD)
Put this in .vimrc
```
filetype plugin on
```

## Make local vim settings (OLD METHOD)
first `cd ~/.vim`. Then `mkdir ftplugin`. cd inside and create a file called
`html.vim`. Inside place any custom vimrc settings using setlocal. To enable 2
tabspace put
```
setlocal ts=2
setlocal sw=2
``` 
inside html.vim 

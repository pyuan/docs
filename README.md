# Docs

This is the table of contents. Feel free to jump anywhere. 

## Arch Install
- [Arch Installation](https://gitlab.com/Peetachip/docs/blob/master/arch/install_arch.md)
- [NetworkManager](https://gitlab.com/Peetachip/docs/blob/master/arch/networkmanager.md)
- [SDDM Configs](https://gitlab.com/Peetachip/docs/blob/master/arch/sddm_config.md)

## Config Window Manager
- [Hermes Config](https://gitlab.com/Peetachip/docs/blob/master/hermesconfig/config.md)
test2

# NetworkManager Guide

## Preparations
Install NetworkManager using a package manager. Then enable the daemon with\
`systemctl enable NetworkManager`

## Normal Use
From [Arch Wiki](https://wiki.archlinux.org/index.php/NetworkManager)

List nearby wifi networks  
`nmcli device wifi list`\
Connect to wifi network\
`nmcli device wifi connect YOUR_SSID password YOUR_PASSWORD`\
If hidden\
`nmcli device wifi connect YOUR_SSID password YOUR_PASSWORD hidden yes`

## WPA-2 Enterprise
[Source](https://askubuntu.com/questions/262491/connect-to-a-wpa2-enterprise-connection-via-cli-no-desktop)\
Do `ip link` to find your wifi interface. Wireless usually begins with w and
ethernet e. An example is wlp3s0. This is called the interface.\
`nmcli con add type wifi ifname YOUR_INTERFACE con-name CONNECTION_NAME ssid
YOUR_SSID`\
To open the nmcli propmt\
`nmcli con edit id CONNECTION_NAME`\
Inside the prompt enter all of these
```
set ipv4.method auto
set 802-1x.eap peap
set 802-1x.phase2-auth mschapv2
set 802-1x.identity YOUR_USERNAME
set 802-1x.password YOUR_PASSWORD
set wifi-sec.key-mgmt wpa-eap
```
Your password and username are the credentials needed to access a colleges
internet or anything using wpa-2 enterprise. Additionally, commands 2, 3, 6 may
be different for each network, so try to look at the security and
authenticaiton certificates.\ 
After this, run
```
save
activate
```
If it configures and connects succesfully, control-c out.

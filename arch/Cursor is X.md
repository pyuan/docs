# Cursor is X

Sometimes the cursor is an x instead of a normal cursor. To fix this add

```
xsetroot -cursor_name left_ptr &
```

inside .xinitrc or bspwmrc. You may need to install xorg-xsetroot.

[Wiki link](https://wiki.archlinux.org/index.php/Cursor_themes#Change_X_shaped_default_cursor)



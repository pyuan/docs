# Guide to Music Player Daemon (MPD), ncmpcpp front end, and Console-base audio
# visualizer (CAVA)
test

## mpd
Mpd needs to be setup before ncmpcpp will work. Mpd files are usually placed in
~/.config/mpd. Copy the mpd config file from
`/usr/share/doc/mpd/mpdconf.example` to ~/.config/mpd. 


Log file and music directory should be uncommented. ~/music is where you will
place tracks. When you add a song run `mpc update` to update mpd of the new
tracks.

## ncmpcpp
ncmpcpp has a config file in ~/.ncmpcpp. Inside a file called
~/.ncmpcpp/config, place these three lines:
```
mpd_host = localhost
mpd_port = 6600
mpd_music_port = ~/music
```
ncmpcpp should now be able to play music.


## cava
Cava accepts output from pulseaudio. Inside ~/.config/mpd/mpd.conf write:
```
audio_output {
	type 		"pulse"
	name		"pulse audio"
}
```
In ~/.config/cava/config write:
```
[input]
; method = pulse
; source = auto
```
There are also many other settings to change, so look at some dotfiles. The
visualizer will go very high at first, but that is just a fourier thing. It
will look fine in a few seconds.

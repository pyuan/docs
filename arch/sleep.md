# How to stop a computer from falling asleep
This is not suspending to ram or hibernation. This aims to stop a computer from
falling asleep after a certain amount inactivity.
In `/etc/X11/xorg.conf.d/10-monitor.conf` inside monitor section add,
```
Option "DPMS" "true"
```
Then add another section
```
Section "ServerLayout0"
	Identifier "ServerLayout0"
	Option "Standbytime" "0"
EndSection
```
These examples came from the arch wiki for `dpms`. See
[archwiki](https://wiki.archlinux.org/index.php/Display_Power_Management_Signaling)

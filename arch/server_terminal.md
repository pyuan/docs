If you can't use backspace in a terminal on a server, it means the server
doesn't recognize the terminal you are using. To fix it:
```
echo 'export TERM=linux' >> ~/.bash_profile
```
on the user of the server.

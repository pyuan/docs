# How to set GTK theme
First install `gtk3` or `gtk2` depending on what software is needed\
Try to see if your preferred gtk theme can be install with pacman or aur.\
## GTK2
In home directory, make a file called `.gtkrc-2.0`. Inside themes can be placed
like this
```
gtk-icon-theme-name = "Adwaita"
gtk-theme-name = "Adwaita"
gtk-font-name = "DejaVu Sans 11"
```
## GTK3
In `~/.config/gtk-3.0` make a file called `settings.ini`
```
[Settings]
gtk-theme-name=Arc-Dark
gtk-icon-theme-name=breeze-dark
gtk-font-name=Sans 11
```

## Manual Install Themes
If a theme cannot be installed through pacamn or AUR, themes go in
`~/.local/share/themes/`. 

## Set theme immediately
```
gsettings set org.gnome.desktop.interface gtk-theme THEME_NAME
```

## Final Notes
All information and examples here comes from [Arch
wiki](https://wiki.archlinux.org/index.php/GTK#Examples)

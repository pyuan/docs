# Arch Installation

**Follow Arch Linux Guide to Install Arch**
https://wiki.archlinux.org/index.php/Installation_guide

1. **Ensure your network interface is listed and enabled, for example with ip-link(8):**\
`ip link`
_________________
2. **Set Up WiFi | WiFi-Menu**\
`wifi-menu`
_________________
If WPA Personal
* Choose Network
* Type Password when Prompted


If WPA Enterprise
* Choose Network
* Go To Location of Created File and Add the Following Code
Link: https://growworkinghard.altervista.org/netctl-wifi-menu-with-wpa2-enterprise-network/

```
Description='Profile description'
Interface=WIRELESS_INTERFACE
Connection=wireless
Security=wpa-configsection
IP=dhcp
ESSID=UNIVERSITY_SSID
WPAConfigSection=(
'ssid="UNIVERSITY_SSID"'
'proto=RSN'
'key_mgmt=WPA-EAP'
'eap=PEAP'
'group=CCMP'
'pairwise=CCMP'
'identity="UNIVERSITY_USERNAME"'
'password="UNIVERSITY_PASSWORD"'
'phase2="auth=MSCHAPV2"'
)
```

* Start netctl with\
`systemctl start netctl`  
`netctl start filename`
_________________
3. **Use timedatectl(1) to ensure the system clock is accurate:**\
`timedatectl set-ntp true`
_________________
4. **Partition the Disk**\
`cfdisk` 
_________________
5. **Mount the Disk**\
`mkfs.ext4 /dev/sda1`\
`mount /dev/sda1 /mnt`
_________________
6. **Install Essential Packages**\
`pacstrap /mnt base linux linux-firmware`
_________________
7. **FStab**\
`genfstab -U /mnt >> /mnt/etc/fstab`
_________________
8. **Change Root**\
`arch-chroot /mnt`
_________________
9. Install Packages for Internet Later\
`pacman -S dialog netctl networkmanager dhcpcd`
_________________
10. **Time Zone**\
`ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime`\
`hwclock --systohc`
_________________
11. **Localization**
*  Uncomment en_US.UTF-8 UTF-8 and other needed locales in /etc/locale.gen

*  Create the locale.conf(5) file, and set the LANG\
`/etc/locale.conf`\
`LANG=en_US.UTF-8`

* Run: 
`locale-gen`
_________________
12. **Hostname**
*  Find File:
`/etc/hostname`
* Add The Name of Your Computer
`myhostname`

*  Find File:
`/etc/hosts`
*  Add the Following Things
```
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
```
_________________
13. **Change Password**
`passwd`
_________________
14. **Install Grub**\
`mkinitcpio -p linux`\
`pacman -S grub os-prober`\
`grub-install /dev/sda`\
`grub-mkconfig -o /boot/grub/grub.cfg`
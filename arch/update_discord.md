# Discord updates
when presented with options to download a .deb or tar file to update discord
run
```
sudo pacman -U https://www.archlinux.org/packages/community/x86_64/discord/download/
```
Usually, you should just update discord through the package manager. It may
take awhile for package mantainers to update.

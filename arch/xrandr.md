# How to configure monitor with xrandr

## Use xrandr to find display outputs
```xrandr```
This prints which display ports are used and is the identifier

If you are using sddm as a display manager, `/usr/share/sddm/scripts/Xsetup` is
where xrandr commands should be placed. 

## Example
```
xrandr --output DP-4 --mode 2560x1440 --rate 165 
xrandr --output HDMI-0 --rotate "right"
```
### For nvidia graphics, screen tearing can be solved by forcing pipeline
Put this in the same file as above. This will require nvidia-settings or the
GUI installed.
```
nvidia-settings --assign CurrentMetaMode="DP-4: nvidia-auto-select +0+0 {ForceFullCompositionPipeline=On}, HDMI-0: nvidia-auto-select +2560+0 {ForceCompositionPipeline=On}"
```
Replace DP-4 and HDMI-0 with your outputs. The +0+0 is the coordinated of the
monitor. I need my second monitor 2560 pixels away from my primary monitor so
no overlapping. I have done ForceFullCompoisitonPipeline for my main monitor to
get rid of all screen tearing.

# Set monitor as primary
Go to `/etc/X11/xorg.conf.d` and create `10-monitor.conf`
Inside write
```
Section "Monitor"
	identifier	"YOUR_IDENTIFIER"
	Option 		"Primary" "true"
EndSection
```

## Another way to force pipeline, but with only one monitor
Inside `/etc/X11/xorg.conf.d` create `20-nvidia.conf`
Inside write
```
Section "Screen"
	Identifier	"DP-4"
	Option		"metamodes" "DP-4: nvidia-auto-select +0+0 {ForceCompositionPipeline=On}" 
	Option 		"AllowIndirectGLXProtocol" "off"
	Option		"TripleBuffer" "on"
EndSection
```

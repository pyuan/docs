# Beep

That annoying sound when you tab complete or do something your computer doesn't
like. It's like a raindrop sound for most distrobutions. To remove it, first
try the methods listed at [arch
wiki](https://wiki.archlinux.org/index.php/PC_speaker).

If those methods don't work, its because there is no single setting to disable
it for everything. The terminal tabbing is where this happens most, so go look
at your terminal config for a setting to disable it. [Kitty terminal's
website](https://sw.kovidgoyal.net/kitty/conf.html?highlight=bell#terminal-bell)
has it clearly listed.

Also check bash or zsh configs for how to turn it off.

Add this to .zshrc
```
unsetopt prompt_cr prompt_cr BEEP
```

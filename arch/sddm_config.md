# SDDM Configuration

## Preparations
Install sddm with a package manager and enable the daemon\
`systemctl enable sddm`\
Install dependencies\
`sudo pacman -S --needed qt5-graphicaleffects qt5-quickcontrols2 qt5-svg`\
Go to the kde store and on the left side find "SDDM Login Themes"\
Find a theme and download the tar file. Inside the Downloads directory run\
`tar -xf TARFILE_NAME`

## Config File
Move the unzipped tarball\
`sudo mv NAME_OF_THEME /usr/share/sddm/themes`\
Create editing folder\
`mkdir /etc/sddm.conf.d`\
Copy the default config file\
`sudo cp -r /usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/default.conf`
Open the default.conf in /etc/sddm.conf.d and find [Theme]
Set Current=NAME_OF_THEME. It is the same name of the directory moved to
/usr/share/sddm/themes\
Reboot and enjoy the login theme.

## fcitx method
https://medium.com/@slmeng/how-to-install-chinese-fonts-and-input-method-in-arch-linux-18b68d2817e7

1. Run the command: `sudo pacman -Syu fcitx fcitx-googlepinyin fcitx-im fcitx-configtool`\
2. Edit ~/.xinitrc
```
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

3. `reboot`
4. `fcitx-config-gtk3`
5. Select Chinese Literature 
6. Ctrl+Space to change between english and chinese


## iBus method
```
sudo pacman -S ibus ibus-libpinyin
```
then add this to `.xinitrc`
```
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
ibus-daemon -d -x
```
and use `ibus-setup` to setup
